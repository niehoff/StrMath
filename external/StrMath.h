#ifdef __cplusplus
extern "C" {
#endif

extern double C_StrMath_Calc(const char*);
extern double C_StrMath_CalcF(const char*, double);

#ifdef __cplusplus
}
#endif
