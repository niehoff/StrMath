unit StrMath;

{$mode objfpc}{$H+}

interface

const
{$ifdef windows}
  strmathlib = 'strmath.dll';
{$else}
  strmathlib = 'libstrmath.so';
{$endif}

function StrMath_Calc(Term: AnsiString):Extended; {$ifdef windows}stdcall;{$else}cdecl;{$endif} external strmathlib;
function StrMath_CalcF(Term: AnsiString; x:Double):Extended; {$ifdef windows}stdcall;{$else}cdecl;{$endif} external strmathlib;

implementation

end.

