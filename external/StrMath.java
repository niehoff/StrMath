public class StrMath{
    static { 
        System.loadLibrary("strmath"); 
    }
    public static native double Calc(String Term);
    public static native double CalcF(String Term, double x);
}
