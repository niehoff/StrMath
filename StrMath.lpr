(*
 * StrMath - a processing library for mathematical expressions in strings
 * Copyright (C) 2013-2022  Rudolf Niehoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *)

library StrMath;

{$mode objfpc}{$H+}

uses
  SysUtils,
  StrMath_Main,
  ctypes,
  JNI;

{$R *.res}

  function JNI_JStringToAnsiString(Env: PJNIEnv; JStr: JString): ansistring;
  var
    IsCopy: JBoolean;
    Chars: PChar;
  begin
    if (JStr = nil) then
    begin
      Result := '';
      Exit;
    end;

    Chars := Env^^.GetStringUTFChars(Env, JStr, @IsCopy);
    if Chars = nil then
      Result := ''
    else
    begin
      Result := UTF8Decode(ansistring(Chars));
      Env^^.ReleaseStringUTFChars(Env, JStr, Chars);
    end;
  end;

  function StrMath_Calc(const Term: ansistring): extended;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
  begin
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(Term);
    StrMath_Calc := mStrMath.Calc;
    mStrMath.Free;
  end;

  function StrMath_CalcF(const Term: ansistring; x: extended): extended;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
    sNumber: string;
  begin
    Str(x, sNumber);
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(StringReplace(Term, 'x', sNumber, [rfReplaceAll, rfIgnoreCase]));
    StrMath_CalcF := mStrMath.Calc;
    mStrMath.Free;
  end;

  //C-Support
  function C_StrMath_Calc(Term: PChar): double;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
  begin
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(ansistring(Term));
    C_StrMath_Calc := mStrMath.Calc;
    mStrMath.Free;
  end;

  function C_StrMath_CalcF(Term: PChar; x: double): double;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
    sNumber: string;
  begin
    Str(x, sNumber);
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(StringReplace(ansistring(Term), 'x', sNumber,
      [rfReplaceAll, rfIgnoreCase]));
    C_StrMath_CalcF := mStrMath.Calc;
    mStrMath.Free;
  end;

  //Java-Support
  function Java_StrMath_Calc(PEnv: PJNIEnv; Obj: JObject; Term: JString): JDouble;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
  begin
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(JNI_JStringToAnsiString(PEnv, Term));
    Java_StrMath_Calc := mStrMath.Calc;
    mStrMath.Free;
  end;

  function Java_StrMath_CalcF(PEnv: PJNIEnv; Obj: JObject; Term: JString;
    x: JDouble): JDouble;
{$ifdef windows}stdcall;{$else}
  cdecl;
{$endif}
  var
    mStrMath: TStrMath;
    sTerm: ansistring;
    sNumber: string;
  begin
    Str(x, sNumber);
    sTerm := StringReplace(JNI_JStringToAnsiString(PEnv, Term), 'x',
      sNumber, [rfReplaceAll, rfIgnoreCase]);
    mStrMath := TStrMath.Create;
    mStrMath.SetTerm(sTerm);
    Java_StrMath_CalcF := mStrMath.Calc;
    mStrMath.Free;
  end;

exports
  StrMath_Calc,
  StrMath_CalcF,
  C_StrMath_Calc,
  C_StrMath_CalcF,
  Java_StrMath_Calc,
  Java_StrMath_CalcF;

begin

end.
