(*
 * StrMath - a processing library for mathematical expressions in strings
 * Copyright (C) 2013-2022  Rudolf Niehoff
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *)

unit StrMath_Main;

{$mode objfpc}

interface

uses SysUtils, Math;

const
  cPI: extended = 3.1415926535897932384626433832795;
  cEU: extended = 2.7182818284590452353602874713526;

type

  TStrMath = class
  private
    number: array of extended;
    Struc: ansistring;

    level: array [char] of byte;

    procedure Term2Struc(const Term: ansistring);
    function Struc2Term: ansistring;

    procedure CalcOperatorStruc(var S: ansistring);
    procedure CalcBracketStruc(var S: ansistring);
    procedure CalcFunction(var Name: ansistring; prmtr: array of ansistring);
    procedure CalcFunctionStruc(var S: ansistring);

  public
    constructor Create;
    destructor Destroy;

    procedure SetTerm(const T: ansistring);
    function GetTerm: ansistring;
    function GetStruc: ansistring;

    function SetNumber(n: extended; i: integer): boolean;
    function GetNumber(i: integer): extended;

    function Calc: extended;
  end;

  PStrMath = Pointer;

implementation

constructor TStrMath.Create;
begin
  inherited Create;

  level['+'] := 1;
  level['-'] := 1;
  level['*'] := 2;
  level['/'] := 2;
  level[#39] := 3;
  level['^'] := 3;
  level['#'] := 3;
  level['n'] := 4;
  level['g'] := 4;
  level['b'] := 4;

  Struc := '[NULL]';
end;

destructor TStrMath.Destroy;
begin
  SetLength(number, 0);

  inherited Destroy;
end;

//Versucht Termstruktur von den Zahlen zu trennen
//Bei konvertierungsfehlern wird die Position in "Struc" abgelegt
procedure TStrMath.Term2Struc(const Term: ansistring);
var
  i, j, pos, Count, eCode: integer;
  b: array [0..4] of boolean;
  part: string;
begin
  Count := 0;
  Struc := '';

  i := 1;
  while i <= length(Term) do
  begin

    b[0] := Term[i] in ['0'..'9'];
    b[1] := Term[i] = '-';
    b[2] := Term[i + 1] in ['0'..'9'];
    if i > 1 then
    begin
      //')'ende eines Ausdrucks, also ja
      b[3] := Term[i - 1] in (['!'..'/'] - [')']);
    end
    else
    begin
      b[3] := True;
    end;

    if b[0] or (b[1] and b[2] and b[3]) then
    begin
      pos := i;

      repeat
        Inc(i);
        b[0] := Term[i] in ['0'..'9'];
        b[1] := (Term[i] = '.') or (Term[i] = ',');
        b[2] := (Term[i] = 'E') or (Term[i] = 'e');
        b[3] := (Term[i] = '-') and ((Term[i - 1] = 'E') or (Term[i - 1] = 'e'));
        b[4] := (Term[i] = '+') and ((Term[i - 1] = 'E') or (Term[i - 1] = 'e'));
      until not (b[0] or b[1] or b[2] or b[3] or b[4]);

      part := Copy(Term, pos, i - pos);
      SetLength(number, Count + 1);
      Val(part, number[Count], eCode);
      if eCode <> 0 then
      begin

        for j := 1 to length(part) do case part[j] of
            '.': part[j] := ',';
            ',': part[j] := '.';
          end;

        Val(part, number[Count], eCode);
        if eCode <> 0 then
        begin
          Struc := '[ERROR]NUMBER pos ' + IntToStr(Count);
          SetLength(number, 0);
          Break;
        end;

      end;

      Struc := Struc + IntToHex(Count, 4);
      Inc(Count);
    end
    else
    begin

      if Term[i] in ['a'..'z', 'A'..'Z'] then
      begin

        //Konstanten
        if (Term[i] = 'l') or (Term[i] = 'L') then
        begin //"log" operator ist g, n, b
          SetLength(number, Count + 1);
          number[Count] := 0.0;
          Struc := Struc + IntToHex(Count, 4);
          i := i + 1;
          Inc(Count);
        end
        else if (Term[i] = 'n') or (Term[i] = 'g') or (Term[i] = 'b') then
        begin //nur Operator
          Struc := Struc + Term[i];
          i := i + 1;
        end
        else if (Term[i] = 'e') or (Term[i] = 'E') then
        begin
          SetLength(number, Count + 1);
          number[Count] := cEU;
          Struc := Struc + IntToHex(Count, 4);
          i := i + 1;
          Inc(Count);
        end
        else if CompareText(Copy(Term, i, 2), 'pi') = 0 then
        begin
          SetLength(number, Count + 1);
          number[Count] := cPI;
          Struc := Struc + IntToHex(Count, 4);
          i := i + 2; //Pi belegt zwei Chars
          Inc(Count);
        end
        else
        begin
          repeat
            Struc := Struc + Term[i];
            Inc(i);
          until not (Term[i] in ['a'..'z', 'A'..'Z']);
          if Term[i] <> '(' then
          begin
            Struc := '[ERROR]TERM pos ' + IntToStr(i);
            SetLength(number, 0);
            Break;
          end;
        end;

      end
      else
      begin
        if Term[i] <> ' ' then Struc := Struc + Term[i];
        Inc(i);
      end;
    end;
  end;

end;

function TStrMath.Struc2Term: ansistring;
var
  i, a: integer;
  part, Term: ansistring;
begin
  i := 1;
  Term := Struc;

  if (Copy(Term, 1, 6) <> '[NULL]') and (Copy(Term, 1, 7) <> '[ERROR]') then
    while i <= length(Term) do
    begin
      if (Term[i] in ['0'..'9']) or (Term[i] in ['A'..'F']) then
      begin
        a := StrToInt(Copy(Term, i, 4));

        Str(number[a], part);

        Delete(Term, i, 4);
        Insert(part, Term, i);

        i := i + length(part);
      end;

      Inc(i);
    end;

  Struc2Term := Term;
end;

//Funktionen zum Verrechnen der Werte nach Strukturangabe

procedure TStrMath.CalcOperatorStruc(var S: ansistring);
var
  oCount, oPos, h, z1, z2: integer;
  i, j: integer;
begin
  //Länge der übergebenen Zeichenkette prüfen, ansonsten Fehler zurückgeben
  //Standard: FFFFoFFFFoFFFFo...   //länge +1 ist durch 5 Teilbar

  if ((Length(S) + 1) mod 5 = 0) and (Copy(S, 1, 7) <> '[ERROR]') and
    (Copy(S, 1, 6) <> '[NULL]') then
  begin

    oCount := (Length(S) + 1) div 5 - 1;

    //Wenn Operatoren vorhanden sind:
    if oCount > 0 then for i := 1 to oCount do
      begin

        oPos := 0;
        h := 0;

        //Operator mit der höchsten Ordnung ermitteln
        for j := 1 to oCount - (i - 1) do if h < level[S[j * 5]] then
          begin
            h := level[S[j * 5]];
            oPos := j * 5;
          end;

        //Herauslösen des zu verrechnenden Teils
        z1 := StrToInt('$' + Copy(S, oPos - 4, 4));
        z2 := StrToInt('$' + Copy(S, oPos + 1, 4));

        //Verrechnen
        case S[oPos] of
          '+': number[z1] := number[z1] + number[z2];
          '-': number[z1] := number[z1] - number[z2];
          '*': number[z1] := number[z1] * number[z2];
          '/': number[z1] := number[z1] / number[z2];
          #39: number[z1] := Power(number[z1], 1 / number[z2]);
          '^': number[z1] := Power(number[z1], number[z2]);
          //#39: number[z1] := Exp(Ln(number[z1]) * number[z2]);
          '#': number[z1] := Logn(number[z1], number[z2]);
          'n': number[z1] := Ln(number[z2]);
          'g': number[z1] := Log10(number[z2]);
          'b': number[z1] := Log2(number[z2]);
          else
            number[z1] := 2;
        end;

        Delete(S, oPos, 5);
      end;
  end;
end;

procedure TStrMath.CalcBracketStruc(var S: ansistring);
var
  OpenBr, CloseBr: integer;
  BrCount, i, j: integer;
  part: ansistring;
begin
  //Anzahl der geöffneten Klammern ermitteln
  BrCount := 0;
  for i := 1 to Length(S) do if S[i] = '(' then Inc(BrCount);


  //Für jede enthaltene Klammer einen Durchlauf
  if BrCount > 0 then for i := 1 to BrCount do
    begin

      //innerste Klammeroperation ermitteln, welche keine weiteren Klammern enthält
      for j := 1 to Length(S) do if S[j] = '(' then OpenBr := j;
      for j := Length(S) downto OpenBr do if S[j] = ')' then CloseBr := j;

      //Zeichenkette herauslösen...
      part := Copy(S, OpenBr + 1, CloseBr - (OpenBr + 1));
      Delete(S, OpenBr, CloseBr - (OpenBr - 1));

      //..verrechnen und Ergebnis einfügen
      CalcOperatorStruc(part);
      Insert(part, S, OpenBr);
    end;

  CalcOperatorStruc(S);
end;

procedure TStrMath.CalcFunction(var Name: ansistring; prmtr: array of ansistring);
var
  i: integer;
  z: array of integer;
begin
  (*WriteLn('test');
  WriteLn(name,length(prmtr));
  for i := 0 to length(prmtr)-1 do begin
    WriteLn(prmtr[i],': ',FloatToStr(number[StrToInt('$'+prmtr[i])]));
  end;
  ReadLn();   *)
  SetLength(z, length(prmtr));
  for i := 0 to length(prmtr) - 1 do z[i] := StrToInt('$' + prmtr[i]);

  Name := AnsiLowerCase(Name); //Groß-/kleinschreibung ignorieren
  case length(prmtr) of
    1: begin

      if Name = 'sin' then
      begin
        number[z[0]] := sin(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'cos' then
      begin
        number[z[0]] := cos(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'tan' then
      begin
        number[z[0]] := tan(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'cotan' then
      begin
        number[z[0]] := cotan(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'arcsin' then
      begin
        number[z[0]] := arcsin(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'arccos' then
      begin
        number[z[0]] := arccos(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'arctan' then
      begin
        number[z[0]] := arctan(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'ln' then
      begin
        number[z[0]] := ln(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'lg' then
      begin
        number[z[0]] := log10(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'lb' then
      begin
        number[z[0]] := log2(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'sqr' then
      begin
        number[z[0]] := sqr(number[z[0]]);
        Name := prmtr[0];
      end
      else if Name = 'sqrt' then
      begin
        number[z[0]] := sqrt(number[z[0]]);
        Name := prmtr[0];
      end
      else
        Name := '[ERROR]';

    end;
    2: begin

      if Name = 'log' then
      begin
        number[z[0]] := logn(number[z[0]], number[z[1]]);
        Name := prmtr[0];
      end
      else if Name = 'power' then
      begin
        number[z[0]] := power(number[z[0]], number[z[1]]);
        Name := prmtr[0];
      end
      else
        Name := '[ERROR]';

    end;
    else
      Name := '[ERROR]';
  end;

end;

procedure TStrMath.CalcFunctionStruc(var S: ansistring);
var
  i, j, pos, bC: integer;
  psbFunc: boolean;
  FuNamePos, FuOpenBr, FuCloseBr, FuSemiCount: integer;
  FuSemi: array of integer;
  FuName: ansistring;
  part: array of ansistring;
begin
  psbFunc := False;
  //(d) <-- funktion min 6zeichen = -2
  for i := Length(S) - 2 downto 1 do
  begin

    //ja, ist eine Funktion
    if (S[i] in ['a'..'z', 'A'..'Z']) and psbFunc then
    begin

      psbFunc := False;
      //Anfang Funktionsname suchen
      pos := i;
      repeat
        FuNamePos := pos;
        Dec(pos);
      until not (S[pos] in ['a'..'z', 'A'..'Z']) or (FuNamePos = 1);

      //richtige Klammer finden
      bC := 1;
      FuSemiCount := 0;
      for j := FuOpenBr + 1 to Length(S) do
      begin
        case s[j] of
          '(': Inc(bC);
          ')': Dec(bC);
          ';': begin
            Inc(FuSemiCount);
            SetLength(FuSemi, FuSemiCount);
            FuSemi[FuSemiCount - 1] := j;
          end;
        end;
        if bC = 0 then
        begin
          FuCloseBr := j;
          Break;
        end;
      end;

      //Parameter berechnen
      SetLength(part, FuSemiCount + 1);
      pos := FuOpenBr;
      for j := 0 to FuSemiCount - 1 do
      begin
        //Zeichenkette herauslösen verrechnen und Ergebnis einfügen
        part[j] := Copy(S, pos + 1, FuSemi[j] - (pos + 1));
        CalcBracketStruc(part[j]);

        pos := FuSemi[j]; //Anfang des nächsten Parameters
      end;
      //Zeichenkette herauslösen verrechnen und Ergebnis einfügen
      part[FuSemiCount] := Copy(S, pos + 1, FuCloseBr - (pos + 1));
      CalcBracketStruc(part[FuSemiCount]);

      //Funktion entgültig berechnen
      FuName := Copy(S, FuNamePos, FuOpenBr - FuNamePos);
      CalcFunction(FuName, part);

      Delete(S, FuNamePos, FuCloseBr - (FuNamePos - 1));
      Insert(FuName, S, FuNamePos);
    end;

    //mögliche Funktion
    if S[i] = '(' then
    begin
      psbFunc := True;
      FuOpenBr := i; //Position Klammer auf merken
    end
    else
      psbFunc := False;
  end;

  CalcBracketStruc(S);
end;

procedure TStrMath.SetTerm(const T: ansistring);
begin
  Term2Struc(T);
end;

function TStrMath.GetTerm: ansistring;
begin
  Result := Struc2Term;
end;

function TStrMath.GetStruc: ansistring;
begin
  Result := Struc;
end;

function TStrMath.SetNumber(n: extended; i: integer): boolean;
begin
  if i < length(number) then
  begin
    number[i] := n;
    SetNumber := True;
  end
  else
  begin
    SetLength(number, i + 1);

    if i < length(number) then
    begin
      number[i] := n;
      SetNumber := True;
    end
    else
      SetNumber := False;
  end;
end;

function TStrMath.GetNumber(i: integer): extended;
begin
  GetNumber := number[i];
end;

function TStrMath.Calc: extended;
begin
  CalcFunctionStruc(Struc);
  Result := number[0];
end;


end.
