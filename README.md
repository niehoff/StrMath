# StrMath

StrMath - a processing library for mathematical expressions in strings
Copyright (C) 2013-2022  Rudolf Niehoff

### Build program

StrMath is written in Free Pascal using the Lazarus IDE and can be compiled under each operating system which is supported also by Lazarus.

1. Install Lazarus IDE: https://www.lazarus-ide.org/index.php
2. Open file StrMath.lpi with Lazarus IDE
3. Create executable file under menu item Run->Build.
