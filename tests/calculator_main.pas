unit Calculator_Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  StrMath in '../external/StrMath.pas';

type

  { TCalculatorForm }

  TCalculatorForm = class(TForm)
    ButtonCalculate: TButton;
    EditExpression: TEdit;
    MemoResults: TMemo;
    procedure ButtonCalculateClick(Sender: TObject);
    procedure EditExpressionKeyPress(Sender: TObject; var Key: char);
  private

  public

  end;

var
  CalculatorForm: TCalculatorForm;

implementation

{$R *.lfm}

{ TCalculatorForm }

procedure TCalculatorForm.ButtonCalculateClick(Sender: TObject);
var
  Result: string;
begin
  try
    Result := FloatToStr(StrMath_Calc(EditExpression.Text));
  except
    Result := '???';
  end;

  MemoResults.Lines.Insert(0, EditExpression.Text + '=' + Result);
end;

procedure TCalculatorForm.EditExpressionKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then // #13 =Enter
  begin
    ButtonCalculateClick(Sender);
  end;
end;

end.
