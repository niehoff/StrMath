#include <string.h>
#include <stdio.h>
#include <StrMath.h>

int main()
{
    char *s;
    s = strdup("5+7*(x-1)");

    double d;

    int i;
    for(i=0;i<5;i++) {
        d = C_StrMath_CalcF(s,i);
        printf("Result from StrMath: %f\n", d);
    }
    return 0;
}
