# Changelog
### StrMath - a processing library for mathematical expressions in strings

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2022-05-24
### Added
- header file for C/C++
- function exports: C_StrMath_Calc, C_StrMath_CalcF
- calculator gui as test program

### Changed
- license is GPLv3 now

## [0.2.0] - 2013-01-27
### Added
- functions could be called (predefined for now)

## [0.1.4] - 2013-01-25
### Added
- constant values like e and pi
- Ln, Lg, Lb for logarithmic operations and # as operator for any base
- operator ^ for Power, operator ' for roots

## [0.1.3] - 2013-01-16
### Added
- 64Bit Version for Windows

### Changed
- decimal point or comma allowed

## [0.1.2] - 2013-01-10
### Added
- no need to write negative numbers as 0-x anymore
- spaces will be removed befor calculation

### Fixed
- fixed country specific error with decimal point and comma

## [0.1.0] - 2013-01-09
### Added
- header files for Pascal and Java
- function exports: StrMath_Calc, StrMath_CalcF, Java_StrMath_Calc, Java_StrMath_CalcF
- Calc and CalcF for calculation with and without variable
- shared library for processing mathematical expressions in strings
